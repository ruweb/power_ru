#!/bin/sh
for file in `ls ../power_user`; do
    if [ ! -e $file ]; then
	ln -sv ../power_user/$file
    fi
done
for file in `ls -d ../power_user/user/*`; do
    file=${file##../power_user/}
    if [ ! -e $file ]; then
        ln -sv ../../power_user/$file $file
    fi
done
for file in `ls -d ../power_user/user/*/*`; do
    file=${file##../power_user/}
    if [ ! -e $file ]; then
        ln -sv ../../../power_user/$file $file
    fi
done

